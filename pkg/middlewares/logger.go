package middlewares

import (
	"log"
	"net/http"
	"time"

	"github.com/go-chi/chi/middleware"

	"gitlab.com/intric-dk/webservices/device-registry/pkg/config"
)

// Logger is a middleware that logs response codes, paths and times.
func Logger() func(next http.Handler) http.Handler {
  cfg := config.Load()

	log.Printf("HTTP loglevel: %d", cfg.HTTPLoglevel)

	return func(next http.Handler) http.Handler {
		fn := func(w http.ResponseWriter, r *http.Request) {
			// Get wrapped response writer and request start.
			ww := middleware.NewWrapResponseWriter(w, r.ProtoMajor)
			start := time.Now()

			// Execute function once the request finishes.
			defer func() {
				if ww.Status() >= cfg.HTTPLoglevel {
					log.Printf("%s %s %d %dB %s\n", r.Method, r.URL.Path, ww.Status(), ww.BytesWritten(), time.Since(start))
				}
			}()

			// Serve middlewares.
			next.ServeHTTP(ww, r)
		}

		// Create handler function.
		return http.HandlerFunc(fn)
	}
}
