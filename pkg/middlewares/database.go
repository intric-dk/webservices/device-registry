package middlewares

import (
	"context"
	"log"
	"net/http"
	"time"

	"gitlab.com/intric-dk/webservices/device-registry/pkg/config"
	"gitlab.com/intric-dk/webservices/device-registry/pkg/models"
	"gitlab.com/intric-dk/webservices/device-registry/pkg/util"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

const (
	// ContextKeyDB is the key to access the database in the context.
	ContextKeyDB = util.ContextKey("database")
)

// Database is a middleware that exposes the database as part of the request context.
func Database() func(next http.Handler) http.Handler {
  // Load configuration.
  cfg := config.Load()

	// Configure client.
  clientOptions := options.Client().ApplyURI(cfg.MongoURI)

	// Create temporary context to verify connection.
	ctx := context.Background()
	client, err := mongo.Connect(ctx, clientOptions)
	if err != nil {
		log.Fatalln("error: creating mongodb client failed")
	}

	// Verify database connection.
	err = client.Ping(ctx, readpref.Primary())
	if err != nil {
		log.Fatalln("error: connecting mongodb client failed")
	}
	database := client.Database(cfg.Service)
	log.Println("Database connected: " + database.Name())

	// Create indexes.
	CreateIndexes(ctx, database)

	// Return a new middleware function.
	return func(next http.Handler) http.Handler {
		// Middleware returns an HTTP handler.
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			// Create mongodb client.
			client, err := mongo.Connect(r.Context(), clientOptions)
			if err != nil {
				log.Fatalln("error: creating mongodb client failed")
			}

			// Inject database into request context.
			ctx := context.WithValue(r.Context(), ContextKeyDB, client.Database(cfg.Service))

			// Call next middleware.
			next.ServeHTTP(w, r.WithContext(ctx))
		})
	}
}

// CreateIndexes will ensure that all necessary indexes are created before the server starts.
func CreateIndexes(ctx context.Context, db *mongo.Database) {
	opts := options.CreateIndexes().SetMaxTime(2 * time.Second)

	// Create device indexes.
	_, err := db.Collection("devices").Indexes().CreateMany(ctx, models.DeviceIndexes, opts)
	if err != nil {
		log.Println(err)
		log.Fatalln("error: creating indexes failed: devices")
	}

	log.Println("Generated indexes!")
}
