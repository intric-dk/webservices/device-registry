package app

import (
	"log"
	"net/http"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/joho/godotenv"

	"gitlab.com/intric-dk/webservices/device-registry/pkg/config"
	"gitlab.com/intric-dk/webservices/device-registry/pkg/handlers"
	"gitlab.com/intric-dk/webservices/device-registry/pkg/middlewares"
	"gitlab.com/intric-dk/webservices/device-registry/pkg/routers"
)

// Start will start the service.
func Start() {
	// Configure logging.
  log.SetFlags(log.LstdFlags | log.LUTC)

	// Load variables from ".env" file.
	err := godotenv.Load()
	if err != nil {
		log.Fatal("error: reading file failed: .env")
  }

  // Load the application configuration.
  cfg := config.Load()

	// Print application name and version.
	log.Println("Service: " + cfg.Service)
  log.Println("Version: " + cfg.Version)

	// Create and configure HTTP server.
	server := &http.Server{
    Addr: ":" + cfg.Port,
		Handler: Router(),
  }

	// Start HTTP server.
	log.Println("Service online: http://localhost:" + cfg.Port)
	log.Fatalf("error: server crashed: %s", server.ListenAndServe())
}

// Router returns the chi router instance of the service.
func Router() *chi.Mux {
  // Load the application configuration.
  cfg := config.Load()

	// Create router.
	router := chi.NewRouter()

	// Load custom middlewares.
	router.Use(middlewares.CORS())
	router.Use(middlewares.Logger())
	router.Use(middlewares.Database())

	// Load chi middlewares.
	router.Use(middleware.RedirectSlashes)
	router.Use(middleware.RequestID)
	router.Use(middleware.RealIP)
	router.Use(middleware.SetHeader("Content-Type", cfg.ContentType))
	router.Use(middleware.Compress(9,  cfg.ContentType))

	// Mount routers and handlers.
	router.Mount("/devices", routers.Device())
	router.Mount("/health", routers.Health())
  router.NotFound(handlers.NotFound())

  return router
}
