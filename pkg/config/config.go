package config

import (
	"io/ioutil"
	"log"
	"net/url"
	"os"
	"strconv"
	"strings"
	"time"
)

var (
	// Service is the name of the service.
	Service = "demo"
	// Version is the version of the service.
	Version = "dev"
)

// Config is the application configuration.
type Config struct {
  Service string
  Version string
  Hostname string
  Port string
  MongoURI string
  ContentType string
  StartedAt string
  HTTPLoglevel int
}

var config *Config

// Load loads the application configuration.
func Load() *Config {
  if config != nil {
    return config
  }

	hostname, err := os.Hostname()
	if err != nil {
		log.Fatalf("error: reading hostname failed: %s", err)
  }

  httpLoglevel, err := strconv.Atoi(GetenvOptional("LOGLEVEL", "400"))

  config = &Config{
    Service: Service,
    Version: Version,
    Hostname: hostname,
    Port: GetenvOptional("PORT", "8080"),
    MongoURI: GetenvMandatory("MONGO_URI"),
    ContentType: "application/json",
    StartedAt: time.Now().UTC().Format(time.RFC3339),
    HTTPLoglevel: httpLoglevel,
  }

  return config
}

// Getenv attempts to get the value of an environment variable from
// a file path. If the value is not a file path, it will use the value.
func Getenv(variable string) string {
	// Get value of environment variable.
	value := os.Getenv(variable)

	// Attempt to parse value of environment variable as file.
	fileURL, err := url.Parse(value)
	if err != nil || fileURL.Scheme != "file" {
		return value
	}

	// Read value from file.
	bytes, err := ioutil.ReadFile(fileURL.Path)
	if err != nil {
		log.Fatalf("error: %s", err)
	}

	return strings.TrimSpace(string(bytes))
}

// GetenvMandatory loads a mandatory environment variable and returns
// its value or terminates the process if the variable is unset.
func GetenvMandatory(variable string) string {
	value := Getenv(variable)
	if value == "" {
		log.Fatal("error: missing environment variable: " + variable)
  }

  return value
}

// GetenvOptional loads an optional environment variable and returns
// its value or the provided default value if the varaible is not set.
func GetenvOptional(variable string, defaultValue string) string {
	value := Getenv(variable)
	if value == "" {
		value = defaultValue
  }

  return value
}
