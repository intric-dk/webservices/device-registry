package util

import (
	"gitlab.com/intric-dk/webservices/device-registry/pkg/config"
)

// ContextKey is a datatype to create context keys.
type ContextKey string

// String casts the context key to a string.
func (key *ContextKey) String() string {
  cfg := config.Load()

	return "gitlab.com/intric-dk/webservices/" + cfg.Service + "." + string(*key)
}
