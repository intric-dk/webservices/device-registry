package util

// Pagination describes the pagination parameters used for a query.
type Pagination struct {
	Offset int64 `json:"offset"`
	Limit  int64 `json:"limit"`
}

// HTTPError is the information about an HTTP error.
type HTTPError struct {
	Title  string `json:"title"`
	Status int    `json:"status"`
	Reason string `json:"reason"`
}

// ErrorPayload is the structure that responses will have if an error occurs.
type ErrorPayload struct {
	Error interface{} `json:"error"`
}

// DataPayload is the structure that responses will have if data is sent occurs.
type DataPayload struct {
	Data       interface{} `json:"data"`
	Pagination interface{} `json:"pagination,omitempty"`
}
