package util

const (
	// ReasonEndpointUnsupported occurs when the endpoint is not part of the specification.
	ReasonEndpointUnsupported = "Endpoint Unsupported"
	// ReasonQueryFailed occurs when the database query fails.
	ReasonQueryFailed = "Query Failed"
	// ReasonJSONInvalid occurs when the JSON payload cannot be parsed.
	ReasonJSONInvalid = "JSON Invalid"
)
