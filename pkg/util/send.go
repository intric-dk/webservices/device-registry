package util

import (
	"encoding/json"
	"log"
	"net/http"
)

// NewHTTPError creates the payload for a new HTTP error.
func NewHTTPError(status int, reason string) *ErrorPayload {
	// Set fallback reason text.
	if reason == "" {
		reason = "Reason Unknown"
	}

	// Return newly created payload.
	return &ErrorPayload{
		HTTPError{
			http.StatusText(status),
			status,
			reason,
		},
	}
}

// SendError sends an HTTP error by using the response writer.
func SendError(w http.ResponseWriter, status int, reason string) {
	// Set status code.
	w.WriteHeader(status)

	// Encode response body.
	err := json.NewEncoder(w).Encode(NewHTTPError(status, reason))
	if err != nil {
		log.Fatalf("error: encoding response error failed: %s", err)
	}
}

// SendEntity sends a data response for a single entity.
func SendEntity(w http.ResponseWriter, status int, data interface{}) {
	// Set status code.
	w.WriteHeader(status)

	// Encode response body.
	if data != nil {
		err := json.NewEncoder(w).Encode(&DataPayload{
			Data: data,
		})
		if err != nil {
			log.Fatalf("error: encoding response data failed: %s", err)
		}
	}
}

// SendEntities sends a data response for multiple entities.
func SendEntities(w http.ResponseWriter, status int, data interface{}, pagination interface{}) {
	// Set status code.
	w.WriteHeader(status)

	// Encode response body.
	if data != nil {
		err := json.NewEncoder(w).Encode(&DataPayload{
			Data:       data,
			Pagination: pagination,
		})
		if err != nil {
			log.Fatalf("error: encoding response data failed: %s", err)
		}
	}
}
