package routers

import (
	"net/url"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPaginateDefault(t *testing.T) {
	// Arrange.
	assert := assert.New(t)
	values, err := url.ParseQuery("")
	assert.Nil(err)

	// Act.
	offset, limit := paginate(values)

	// Assert.
	assert.Equal(int64(0), offset)
	assert.Equal(int64(100), limit)
}

func TestPaginateText(t *testing.T) {
	// Arrange.
	assert := assert.New(t)
	values, err := url.ParseQuery("offset=a&limit=b")
	assert.Nil(err)

	// Act.
	offset, limit := paginate(values)

	// Assert.
	assert.Equal(int64(0), offset)
	assert.Equal(int64(100), limit)
}

func TestPaginateNumbers(t *testing.T) {
	// Arrange.
	assert := assert.New(t)
	values, err := url.ParseQuery("offset=200&limit=400")
	assert.Nil(err)

	// Act.
	offset, limit := paginate(values)

	// Assert.
	assert.Equal(int64(200), offset)
	assert.Equal(int64(400), limit)
}
