package routers

import (
	"encoding/json"
	"net/http"

	"github.com/go-chi/chi"
	"gitlab.com/intric-dk/webservices/device-registry/pkg/config"
	"gitlab.com/intric-dk/webservices/device-registry/pkg/util"
)

// health holds the cached reponse for the application server health.
var health HealthData

// HealthData provides information about the application server health.
type HealthData struct {
	Hostname  string `json:"hostname"`
	Version   string `json:"version"`
	StartedAt string `json:"started_at"`
}

// ListHealth displays application server information.
func ListHealth(w http.ResponseWriter, r *http.Request) {
  json.NewEncoder(w).Encode(&util.DataPayload{
    Data: health,
  })
}

// Health returns a router for the service.
func Health() *chi.Mux {
  // Load application configuration.
  cfg := config.Load()

	// Cache application health.
	health = HealthData{
		Version: cfg.Version,
		Hostname: cfg.Hostname,
		StartedAt: cfg.StartedAt,
	}

	router := chi.NewRouter()

  // Configure route handlers.
	router.Get("/", ListHealth)

	return router
}
