package routers

import (
	"net/url"
	"strconv"
)

func paginate(values url.Values) (int64, int64) {
	offset, err := strconv.ParseInt(values.Get("offset"), 10, 64)
	if err != nil {
		offset = 0
	}
	limit, err := strconv.ParseInt(values.Get("limit"), 10, 64)
	if err != nil {
		limit = 100
	}

	return offset, limit
}
