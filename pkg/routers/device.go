package routers

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/go-chi/chi"
	"github.com/google/uuid"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"

	"gitlab.com/intric-dk/webservices/device-registry/pkg/middlewares"
	"gitlab.com/intric-dk/webservices/device-registry/pkg/models"
	"gitlab.com/intric-dk/webservices/device-registry/pkg/util"
)

// Device returns a router for the device resource.
func Device() chi.Router {
	router := chi.NewRouter()
	collection := "devices"

	// Get all entities of this resource.
	// Commonly referred to as FIND operation.
	router.Get("/", func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()

		// Get pagination parameters.
		offset, limit := paginate(r.URL.Query())

		// Get collection.
		coll := ctx.Value(middlewares.ContextKeyDB).(*mongo.Database).Collection(collection)

		// Find documents.
		cursor, err := coll.Find(ctx, bson.M{}, options.Find().SetSkip(offset).SetLimit(limit))
		if err != nil {
			util.SendError(w, http.StatusInternalServerError, util.ReasonQueryFailed)
			log.Print(err)
			return
		}

		// Decode queried data.
		devices := make([]models.Device, 0)
		err = cursor.All(ctx, &devices)
		if err != nil {
			util.SendError(w, http.StatusInternalServerError, util.ReasonQueryFailed)
			log.Print(err)
			return
		}

		// Send response.
		util.SendEntities(w, http.StatusOK, devices, util.Pagination{
			Offset: offset,
			Limit:  limit,
		})
		return
	})

	// Create an entity of this resource.
	// Commonly referred to as CREATE operation.
	router.Post("/", func(w http.ResponseWriter, r *http.Request) {
		// Parse request payload.
		device := models.Device{}

		// Decode request body.
		if err := json.NewDecoder(r.Body).Decode(&device); err != nil {
			util.SendError(w, http.StatusBadRequest, util.ReasonJSONInvalid)
			return
		}

		// Set device ID.
		uuid, err := uuid.NewRandom()
		if err != nil {
			util.SendError(w, http.StatusInternalServerError, util.ReasonQueryFailed)
			return
		}
		device.ID = uuid.String()

		// Get context.
		ctx := r.Context()

		// Get database.
		coll := ctx.Value(middlewares.ContextKeyDB).(*mongo.Database).Collection(collection)

		// Create new device.
		res, err := coll.InsertOne(ctx, device)
		if err != nil {
			util.SendError(w, http.StatusInternalServerError, util.ReasonQueryFailed)
			log.Print(err)
			return
		}
		device.OID = res.InsertedID.(primitive.ObjectID)

		// Send response.
		util.SendEntity(w, http.StatusOK, device)
		return
	})

	return router
}
