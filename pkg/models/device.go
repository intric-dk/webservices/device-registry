package models

import (
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// DeviceHardware describes the hardware properties of an IoT device.
type DeviceHardware struct {
	Vendor   string `json:"vendor,omitempty" bson:"vendor,omitempty"`
	Platform string `json:"platform,omitempty" bson:"platform,omitempty"`
	Model    string `json:"model,omitempty" bson:"model,omitempty"`
}

// DeviceOperatingSystem describes the operating system properties of an IoT device.
type DeviceOperatingSystem struct {
	Name    string `json:"name,omitempty" bson:"name,omitempty"`
	Version string `json:"version,omitempty" bson:"version,omitempty"`
	Release string `json:"release,omitempty" bson:"release,omitempty"`
}

// Device is an IoT device that is managed via the platform.
type Device struct {
	OID             primitive.ObjectID    `json:"-" bson:"_id,omitempty"`
	ID              string                `json:"id,omitempty" bson:"id,omitempty"`
	SerialNumber    string                `json:"serial_number,omitempty" bson:"serial_number,omitempty"`
	Hardware        DeviceHardware        `json:"hardware,omitempty" bson:"hardware,omitempty"`
	OperatingSystem DeviceOperatingSystem `json:"operating_system,omitempty" bson:"operating_system,omitempty"`
	Status          string                `json:"status,omitempty" bson:"status,omitempty"`
	CertificateHref string                `json:"certificate_href,omitempty" bson:"certificate_href,omitempty"`
	ProjectHref     string                `json:"project_href,omitempty" bson:"project_href,omitempty"`
}

// DeviceIndexes describes the indexes for the model.
var DeviceIndexes = []mongo.IndexModel{
	{
		Keys:    bson.D{bson.E{Key: "id", Value: 1}},
		Options: options.Index().SetUnique(true),
	},
	{
		Keys:    bson.D{bson.E{Key: "serial_number", Value: 1}},
		Options: options.Index().SetUnique(true),
	},
}
