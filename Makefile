.PHONY: run dependencies build test update download keypair docker
REMOTE=gitlab.com
GROUP=intric-dk/webservices
PROJECT=device-registry
REGISTRY=registry.$(REMOTE)
VERSION?=dev
ARCH?=amd64
MONGO_URI?=$(shell cat .env | grep MONGO_URI | sed -e s/^MONGO_URI=//)
BRANCH=$(shell git rev-parse --abbrev-ref HEAD)

# Set GOARCH and GOARM values for cross-compilation.
ifeq ($(ARCH),amd64)
	GOARCH = amd64
endif

ifeq ($(ARCH),armv7hf)
	GOARCH = arm
	GOARM = 7
endif

ifeq ($(ARCH),aarch64)
	GOARCH = arm64
endif

run:
	CGO_ENABLED=0 GOARCH=$(GOARCH) GOARM=$(GOARM) go run -ldflags "-X $(REMOTE)/$(GROUP)/$(PROJECT)/pkg/config.Version=$(VERSION) -X $(REMOTE)/$(GROUP)/$(PROJECT)/pkg/config.Service=$(PROJECT)" cmd/$(PROJECT)/main.go

dependencies:
	CGO_ENABLED=0 GOARCH=$(GOARCH) GOARM=$(GOARM) go mod download

build:
	CGO_ENABLED=0 GOARCH=$(GOARCH) GOARM=$(GOARM) go build -o ./$(PROJECT)-$(ARCH) -ldflags "-s -w -X $(REMOTE)/$(GROUP)/$(PROJECT)/pkg/info.Version=$(VERSION) -X $(REMOTE)/$(GROUP)/$(PROJECT)/pkg/info.Service=$(PROJECT)" cmd/$(PROJECT)/main.go

test:
	CGO_ENABLED=0 GOARCH=$(GOARCH) GOARM=$(GOARM) MONGO_URI=$(MONGO_URI) go test ./...

update:
	rm go.*
	go mod init $(REMOTE)/$(GROUP)/$(PROJECT)
	CGO_ENABLED=0 GOARCH=$(GOARCH) GOARM=$(GOARM) go test ./...
	go mod tidy

download:
	wget -q --show-progress https://$(REMOTE)/$(GROUP)/$(PROJECT)/-/jobs/artifacts/$(BRANCH)/raw/$(PROJECT)-$(ARCH)?job=build-$(ARCH) -O $(PROJECT)-$(ARCH)
	chmod +x $(PROJECT)-$(ARCH)

keypair:
	mkdir certs
	openssl ecparam -genkey -name prime256v1 -noout -out certs/ec_private.pem 2> /dev/null
	openssl ec -in certs/ec_private.pem -pubout -out certs/ec_public.pem 2> /dev/null

docker:
	docker build --build-arg VERSION=$(VERSION) --build-arg ARCH=$(ARCH) -t $(REGISTRY)/$(GROUP)/$(PROJECT):$(VERSION)-$(ARCH) .
