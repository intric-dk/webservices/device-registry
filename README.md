# Device registry

A microservice to manage device information, such as connectivity information and client certificates.

# Usage

Ensure that you have at least **go1.13** and **build-essential** installed and run `make` to compile and run the application. To create a production build, run `make build`.

# License

This project is licensed under the terms of the [MIT license](./LICENSE.md).
