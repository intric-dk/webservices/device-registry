module gitlab.com/intric-dk/webservices/device-registry

go 1.15

require (
	github.com/go-chi/chi v4.1.2+incompatible
	github.com/go-chi/cors v1.1.1
	github.com/google/uuid v1.1.2
	github.com/joho/godotenv v1.3.0
	github.com/stretchr/testify v1.6.1
	go.mongodb.org/mongo-driver v1.4.1
)
