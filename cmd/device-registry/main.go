package main

import (
	"gitlab.com/intric-dk/webservices/device-registry/pkg/app"
)

func main() {
	// Start webservice.
	app.Start()
}
